describe('calendar', () => {
  let page;
  const now = new Date();

  beforeAll(async () => {
    page = await global.__BROWSER__.newPage();
    await page.goto('http://localhost:3000/');
  });

  afterAll(async () => {
    await page.close()
  });

  it('should correctly display grid', async () => {
    const gridLen = await page.$$eval('.cal-grid', elms => elms.length);
    expect(gridLen).toEqual(1);
    const cellLen = await page.$$eval('.cal-cell', elms => elms.length);
    expect(cellLen).toBeGreaterThanOrEqual(35);
  });

  it('should correctly handle month selector click', async () => {
    const label = await page.$eval('.selector span', elm => elm.textContent);
    const prevMonthButton = await page.$('.selector button');
    await prevMonthButton.click();
    const nextLabel = await page.$eval('.selector span', elm => elm.textContent);
    expect(label != nextLabel).toBeTruthy();
  });
});
