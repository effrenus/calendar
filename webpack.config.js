const webpack = require('webpack');
const path = require('path');

const SRC_PATH = path.join(__dirname, './src');
const isProductionMode = process.env.NODE_ENV === 'production';
const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'runtime'
  })
];

if (isProductionMode)
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        unused: true,
        dead_code: true,
        warnings: false,
        screw_ie8: true
      }
    })
  )

module.exports = {
  entry: {
    main: './src/index.js'
  },

  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    path: path.resolve(__dirname, './static'),
    publicPath: '/static/'
  },

  devtool: isProductionMode ? 'source-map' : 'eval',

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: path.resolve(__dirname, '../')
      }
    ]
  },

  resolve: {
    alias: {
      '~': path.resolve(__dirname, 'src/')
    },
    modules: [SRC_PATH, 'node_modules'],
    extensions: ['.js']
  },

  plugins
};
