const fs = require('fs');
const compress = require('koa-compress');
const serve = require('koa-static');
const Koa = require('koa');

const app = new Koa();

app.use(compress({
  filter: function (content_type) {
    return /(javascript|css|json)/i.test(content_type)
  },
  threshold: 2048,
  flush: require('zlib').Z_SYNC_FLUSH
}));

app.use(serve(__dirname));

const router = require('koa-router')();

router
  .get('/', index);

app.use(router.routes());

function index(ctx) {
  ctx.res.statusCode = 200;
  ctx.res.end(fs.readFileSync('./index.html'));
}

app.listen(process.env.PORT || 3000);
