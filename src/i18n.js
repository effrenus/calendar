// @flow
const translations = {
  ru: {
    'month0': 'январь',
    'month1': 'февраль',
    'month2': 'март',
    'month3': 'апрель',
    'month4': 'май',
    'month5': 'июнь',
    'month6': 'июль',
    'month7': 'август',
    'month8': 'сентябрь',
    'month9': 'октябрь',
    'month10': 'ноябрь',
    'month11': 'декабрь',
    'fastinput.add': 'Добавить',
    'fastinput.placeholder': '7 августа, Купить молоко',
    'form.add': 'Готово',
    'form.delete': 'Удалить',
    'form.placehoder.title': 'Событие',
    'form.placehoder.participants': 'Участники',
    'form.placehoder.description': 'Описание'
  }
};

let lang = 'ru';

export function getText(id: string): string {
  return translations[lang][id] || '';
}
