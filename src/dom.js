// @flow
function isEventProp(eventName) {
  return [
    'click', 'submit'
  ].includes(eventName.substring(2).toLowerCase());
}

type Props = {
  [key: string]: string|number|Function,
  className?: string
};

type Children =
  | string
  | Array<?HTMLElement>
  | HTMLElement;

export function createElement(type: string, children: ?Children, props: ?Props) {
  const elm = document.createElement(type);

  if (typeof children === 'string') {
    elm.appendChild(document.createTextNode(children));
  } else if (Array.isArray(children)) {
    children.forEach(child => child && elm.appendChild(child));
  } else if (children instanceof HTMLElement) {
    elm.appendChild(children);
  }

  if (props) {
    Object.keys(props).forEach(propKey => {
      if (isEventProp(propKey) && props && typeof props[propKey] === 'function') {
        // $FlowFixMe
        elm.addEventListener(propKey.substring(2).toLowerCase(), props[propKey]);
      }
      if (['type', 'value', 'placeholder', 'name'].includes(propKey)) {
        // $FlowFixMe
        elm[propKey] = props[propKey];
      }
    });

    if (props.className) {
      elm.className = props.className;
    }
  }

  return elm;
}

export function main(children: ?Children, props: ?Props) {
  return createElement('main', children, props);
}

export function form(children: ?Children, props: ?Props) {
  return createElement('form', children, props);
}

export function input(children: ?Children, props: ?Props) {
  return createElement('input', children, props);
}

export function span(children: ?Children, props: ?Props) {
  return createElement('span', children, props);
}

export function div(children: ?Children, props: ?Props) {
  return createElement('div', children, props);
}

export function b(children: ?Children, props: ?Props) {
  return createElement('b', children, props);
}

export function button(children: ?Children, props: ?Props) {
  return createElement('button', children, props);
}

export function h3(children: ?Children, props: ?Props) {
  return createElement('h3', children, props);
}
