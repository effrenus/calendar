// @flow
import * as calendarStore from '~/store';
import Controller from '~/controller';

const SAVE_INTERVAL = 10000; // ms

const initialState = localStorage.getItem(calendarStore.STORAGE_KEY);
let store = calendarStore.create();

if (initialState) {
  store = calendarStore.rehydrate(store, initialState);
}

const ctrl = new Controller(store);
ctrl.render();

setInterval(() => ctrl.save(), SAVE_INTERVAL);
