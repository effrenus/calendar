// @flow
import * as calendarStore from '~/store';
import * as page from '~/views/page';
import * as selector from '~/views/date-selector';
import * as grid from '~/views/calendar-grid';
import type { Store, Actions } from '~/types';
export default class Controller {
  store: Store;
  send: (action: Actions) => void;
  view: HTMLElement;
  selectorElm: ?HTMLElement;
  gridElm: ?HTMLElement;
  editForm: ?HTMLElement;

  constructor(store: Store) {
    this.store = store;
    this.send = this.send.bind(this);
    this.setup();
  }

  setup() {
    const { store, send } = this;
    this.view = page.create(null, { store, send, formatKeyForDate: calendarStore.formatKeyForDate });
    this.selectorElm = page.find(this.view, selector.ELEMENT_SELECTOR);
    this.gridElm = page.find(this.view, grid.ELEMENT_SELECTOR);
  }

  send(action: Actions) {
    const store = this.store = calendarStore.reduce(this.store, action);

    switch (action.type) {
      case 'NEXT_MONTH':
      case 'PREV_MONTH':
      case 'FAST_INPUT':
        selector.updateText(this.selectorElm, store.currentDate);
        grid.updateGrid(this.gridElm, { date: store.currentDate, store: this.store, findEventForDate: calendarStore.findEventForDate });
        break;
      case 'ADD_OR_EDIT':
        if (this.editForm) {
          grid.closeForm(this.editForm);
        }
        this.editForm = grid.openForm(this.gridElm, { cellIndex: action.index, event: calendarStore.findEventForDate(store, action.date), send: this.send });
        break;
      case 'SAVE':
      case 'EDIT':
        grid.updateCell(this.gridElm, { cellIndex: action.cellIndex, event: calendarStore.findEventForDate(store, action.date) });
        grid.closeForm(this.editForm);
        this.editForm = null;
        break;
      case 'DELETE':
        grid.clearCell(this.gridElm, { cellIndex: action.cellIndex });
        grid.closeForm(this.editForm);
        this.editForm = null;
        break;
    }
  }

  save() {
    localStorage.setItem(calendarStore.STORAGE_KEY, JSON.stringify(this.store));
  }

  render() {
    const rootElm = document.getElementById('root');
    if (rootElm) {
      rootElm.appendChild(this.view);
    }
  }
}
