// @flow
import { div, main } from '~/dom';
import { create as selector } from '~/views/date-selector';
import { create as calendar } from '~/views/calendar-grid';
import { create as fastInput } from '~/views/fast-input';
import type { ActionDispatcher, Store } from '~/types';
import typeof { formatKeyForDate } from '~/store';

type Args = {
  send: ActionDispatcher,
  store: Store,
  formatKeyForDate: formatKeyForDate,
};

export function create(children: any, { store, send, formatKeyForDate }: Args) {
  return div(
    [
      main(
        [
          fastInput(null, { send }),
          selector(null, { date: store.currentDate, send }),
          calendar(null, {
            date: store.currentDate,
            events: store.events,
            send,
            formatKeyForDate,
          }),
        ],
        null
      ),
    ],
    { className: 'page' }
  );
}

export function find(elm: HTMLElement, selector: string) {
  return elm.querySelector(selector);
}
