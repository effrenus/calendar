// @flow
import { div, button, span } from '~/dom';
import { getText as t } from '~/i18n';
import type { ActionDispatcher } from '~/types';

const CLASSNAMES = {
  SELECTOR: 'selector',
  LABEL: 'selector-label',
};

export const ELEMENT_SELECTOR = `.${CLASSNAMES.SELECTOR}`;

function formatDate(d) {
  return `${t(`month${d.getMonth()}`)} ${d.getFullYear()}`;
}

export function create(
  _children: any,
  { date, send }: { date: Date, send: ActionDispatcher }
) {
  return div(
    [
      button('<', { onClick: () => send({ type: 'PREV_MONTH' }) }),
      span(formatDate(date), { className: CLASSNAMES.LABEL }),
      button('>', { onClick: () => send({ type: 'NEXT_MONTH' }) }),
    ],
    { className: CLASSNAMES.SELECTOR }
  );
}

export function updateText(elm: ?HTMLElement, date: Date) {
  const labelElm = elm && elm.querySelector(`.${CLASSNAMES.LABEL}`);
  if (labelElm) {
    labelElm.textContent = formatDate(date);
  }
}
