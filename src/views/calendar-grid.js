// @flow
import { div, button, form, input, h3 } from '~/dom';
import { generateCells } from '~/utils/calendar';
import * as cell from '~/views/calendar-cell';
import { getText as t } from '~/i18n';
import type { Event, ActionDispatcher, Store, Events } from '~/types';
import typeof { findEventForDate, formatKeyForDate } from '~/store';

const CLASSNAMES = {
  ELEMENT: 'cal-grid',
};
export const ELEMENT_SELECTOR = `.${CLASSNAMES.ELEMENT}`;

function addForm({ event, send }) {
  if (!event) event = {};
  return form(
    [
      input(null, {
        value: event.title || '',
        placeholder: t('form.placehoder.title'),
        type: 'text',
        name: 'title',
      }),
      input(null, {
        value: (event.participants && event.participants.join(', ')) || '',
        placeholder: t('form.placehoder.participants'),
        name: 'participants',
      }),
      input(null, {
        value: event.description || '',
        placeholder: t('form.placehoder.description'),
        name: 'description',
      }),
      div(
        [
          button(t('form.add'), { type: 'submit' }),
          button(t('form.delete'), {
            onClick: evt => {
              const {
                index,
                date,
              } = evt.target.parentNode.parentNode.parentNode.dataset;
              send({ type: 'DELETE', cellIndex: index, date: new Date(date) });
            },
            type: 'button',
          }),
        ],
        null
      ),
      button('x', {
        className: 'close',
        onClick: evt => closeForm(evt.target.parentNode),
      }),
    ],
    {
      onSubmit: evt => {
        evt.preventDefault();
        const { index, date } = evt.target.parentNode.dataset;
        const formFields = new FormData(evt.target);
        const title = formFields.get('title');
        if (title && typeof title === 'string' && title.trim()) {
          send({
            type: 'SAVE',
            cellIndex: index,
            data: new FormData(evt.target),
            date: new Date(date),
          });
        } else {
          const errElm = div('Укажите заголовок', { className: 'error' });
          evt.target.prepend(errElm);
          setTimeout(() => errElm.remove(), 3000);
        }
      },
      action: '.',
      className: 'edit-form',
    }
  );
}

function editForm({ event, send }) {
  if (!event) event = {};
  return form(
    [
      h3(event.title),
      event.participants && div(event.participants.join(', ')),
      input(null, {
        value: event.description || '',
        placeholder: t('form.placehoder.description'),
        name: 'description',
      }),
      div(
        [
          button(t('form.add'), { type: 'submit' }),
          button(t('form.delete'), {
            onClick: evt => {
              const {
                index,
                date,
              } = evt.target.parentNode.parentNode.parentNode.dataset;
              send({ type: 'DELETE', cellIndex: index, date: new Date(date) });
            },
            type: 'button',
          }),
        ],
        null
      ),
      button('x', {
        className: 'close',
        onClick: evt => closeForm(evt.target.parentNode),
      }),
    ],
    {
      onSubmit: evt => {
        evt.preventDefault();
        const { index, date } = evt.target.parentNode.dataset;
        send({
          type: 'EDIT',
          cellIndex: index,
          data: new FormData(evt.target),
          date: new Date(date),
        });
      },
      action: '.',
      className: 'edit-form',
    }
  );
}

export function create(
  _children: any,
  {
    date,
    events,
    send,
    formatKeyForDate,
  }: {
    date: Date,
    events: Events,
    send: ActionDispatcher,
    formatKeyForDate: formatKeyForDate,
  }
) {
  const cells = generateCells(date).map((item, index) =>
    cell.create({
      ...item,
      index,
      event:
        events[formatKeyForDate(new Date(item.year, item.month, item.day))],
    })
  );

  return div(cells, {
    className: CLASSNAMES.ELEMENT,
    onClick: event => {
      if (!event.target.classList.contains('cal-cell')) {
        return;
      }
      const { index, date, isEmpty } = event.target.dataset;
      send({ type: 'ADD_OR_EDIT', index, date: new Date(date) });
    },
  });
}

function getCell(grid: HTMLElement, index: number) {
  return grid.querySelectorAll(cell.CELL_SELECTOR)[index];
}

export function updateGrid(
  grid: ?HTMLElement,
  {
    date,
    store,
    findEventForDate,
  }: { date: Date, store: Store, findEventForDate: findEventForDate }
) {
  if (!grid) {
    return;
  }
  const cells = generateCells(date);
  const cellElms = grid.querySelectorAll(cell.CELL_SELECTOR);
  const maxLen = Math.max(cells.length, cellElms.length);
  for (let i = 0; i < maxLen; i++) {
    if (!cells[i]) {
      cellElms[i].remove();
      continue;
    }

    const cellData = cells[i];
    const cellDate = new Date(cellData.year, cellData.month, cellData.day);
    const event = findEventForDate(store, cellDate);
    let cellElm = cellElms[i]
      ? cellElms[i]
      : cell.create({ ...cellData, index: i, event });
    if (!cellElms[i]) {
      grid.appendChild(cellElm);
    }
    cellElm.textContent = String(cellData.day);
    cellElm.dataset.index = String(i);
    cellElm.dataset.date = cellDate.toJSON();

    if (event) {
      cell.updateEventList(cellElm, event);
    } else {
      cell.clearEventList(cellElm);
    }
  }
}

export function updateCell(
  grid: ?HTMLElement,
  { cellIndex, event }: { cellIndex: number, event: ?Event }
) {
  grid && cell.updateEventList(getCell(grid, cellIndex), event);
}

export function clearCell(
  grid: ?HTMLElement,
  { cellIndex }: { cellIndex: number }
) {
  grid && cell.clearEventList(getCell(grid, cellIndex));
}

export function openForm(
  grid: ?HTMLElement,
  {
    cellIndex,
    event,
    send,
  }: { cellIndex: number, event: ?Event, send: ActionDispatcher }
) {
  if (!grid) {
    return;
  }
  const cellElm = getCell(grid, cellIndex);
  const form =
    !cellElm.dataset.isEmpty || cellElm.dataset.isEmpty == 'true'
      ? addForm({ event, send })
      : editForm({ event, send });
  cellElm.appendChild(form);

  return form;
}

export function closeForm(form: ?HTMLElement) {
  form && form.remove();
}
