// @flow
import { div, b } from '~/dom';
import type { Event } from '~/types';

export const CELL_SELECTOR = '.cal-cell';
const CONTENT_CLASSNAME = 'cal-content';

export function create({
  index,
  year,
  month,
  day,
  event,
}: {
  index: number,
  year: number,
  month: number,
  day: number,
  event: ?Event,
}) {
  const cell = div(`${day}`, { className: 'cal-cell' });
  cell.dataset.index = String(index);
  cell.dataset.date = new Date(year, month, day).toJSON();
  if (event) {
    updateEventList(cell, event);
  }

  return cell;
}

export function updateEventList(elm: HTMLElement, event: ?Event) {
  if (!event) {
    return;
  }
  let contentElm = elm.querySelector(`.${CONTENT_CLASSNAME}`);
  if (contentElm) {
    contentElm.remove();
  }

  contentElm = div(null, { className: CONTENT_CLASSNAME });

  const eventElm = div(
    [
      b(event.title),
      event.participants && div(event.participants.join(', ')),
      div(event.description ? event.description : ''),
    ],
    null
  );

  elm.dataset.isEmpty = 'false';
  contentElm.appendChild(eventElm);
  elm.appendChild(contentElm);
  elm.classList.add('active');
}

export function clearEventList(elm: HTMLElement) {
  let contentElm = elm.querySelector(`.${CONTENT_CLASSNAME}`);
  contentElm && contentElm.remove();
  elm.dataset.isEmpty = 'true';
  elm.classList.remove('active');
}
