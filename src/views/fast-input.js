// @flow
import { div, input, button, form } from '~/dom';
import { getText as t } from '~/i18n';
import { parseRawEventString } from '~/utils/calendar';
import type { ActionDispatcher } from '~/types';

const CLASSNAMES = {
  MAIN: 'fast-input',
  WRAP: 'fast-wrap',
};

function onSubmit(evt: Event, send: ActionDispatcher) {
  evt.preventDefault();
  const target = (evt.target: any);
  const inputElm: HTMLInputElement | null = target.querySelector('input');

  if (!inputElm || !inputElm.value.trim()) return;
  const value = inputElm.value;
  const parsedValue = parseRawEventString(value);
  if (!parsedValue) {
    return;
  }

  send({ type: 'FAST_INPUT', ...parsedValue });

  inputElm.value = '';
  toggle(target);
}

function toggle(parent: HTMLElement) {
  const inputWrapELm = parent.querySelector(`.${CLASSNAMES.WRAP}`);
  inputWrapELm && inputWrapELm.classList.toggle('hidden');
}

export function create(_: any, { send }: { send: ActionDispatcher }) {
  return form(
    [
      button(t('fastinput.add'), {
        onClick: evt => toggle(evt.target.parentNode),
        type: 'button',
      }),
      div(input(null, { value: '', placeholder: t('fastinput.placeholder') }), {
        className: `${CLASSNAMES.WRAP} hidden`,
      }),
    ],
    { onSubmit: evt => onSubmit(evt, send), className: CLASSNAMES.MAIN }
  );
}
