// @flow
import type { Store, Event, Actions } from '~/types';

export const STORAGE_KEY = '$calendar$store';

export function create(): Store {
  const now = new Date(Date.now());
  now.setDate(1);
  return { currentDate: now, events: {} };
}

export function rehydrate(store: Store, jsonString: string): Store {
  const json: Store = JSON.parse(jsonString);
  return {
    ...store,
    ...json,
    currentDate: new Date(json.currentDate || store.currentDate)
  };
}

export function findEventForDate(store: Store, date: Date): Event | null {
  const key = `_${date.getFullYear()}_${date.getMonth()}_${date.getDate()}`;
  return store.events[key];
}

function getNextMonthDate(currentDate: Date): Date {
  const nextDate = new Date();
  nextDate.setFullYear(currentDate.getMonth() === 11 ? currentDate.getFullYear() + 1 : currentDate.getFullYear());
  nextDate.setMonth((currentDate.getMonth() + 1) % 12);
  nextDate.setDate(1);
  return nextDate;
}

function getPrevMonthDate(currentDate: Date): Date {
  const nextDate = new Date();
  const month = currentDate.getMonth();
  nextDate.setFullYear(month === 0 ? currentDate.getFullYear() - 1 : currentDate.getFullYear());
  nextDate.setMonth(month === 0 ? 11 : month - 1);
  nextDate.setDate(1);
  return nextDate;
}

export function formatKeyForDate(date: Date): string {
  return `_${date.getFullYear()}_${date.getMonth()}_${date.getDate()}`;
}

function parseFormData(data: FormData): Event {
  const title = data.get('title'),
        participants = data.get('participants'),
        description = data.get('description');
  return {
    title: title && typeof title === 'string' ? title : 'Unknown title',
    participants: participants && typeof participants === 'string' ? participants.split(',').map(s => s.trim()) : null,
    description: typeof description === 'string' ? description : null,
  };
}

export function reduce(store: Store, action: Actions): Store {
  switch (action.type) {
    case 'NEXT_MONTH':
      return {
        ...store,
        currentDate: getNextMonthDate(store.currentDate)
      };
    case 'PREV_MONTH':
      return {
        ...store,
        currentDate: getPrevMonthDate(store.currentDate)
      };
    case 'SAVE':
      return {
        ...store,
        events: {
          ...store.events,
          [formatKeyForDate(action.date)]: parseFormData(action.data)
        }
      };
    case 'EDIT':
      return {
        ...store,
        events: {
          ...store.events,
          [formatKeyForDate(action.date)]: {
            ...store.events[formatKeyForDate(action.date)],
            description: action.data.get('description')
          }
        }
      };
    case 'DELETE':
      delete store.events[formatKeyForDate(action.date)];
      return {
        ...store,
        events: {
          ...store.events
        }
      };
    case 'FAST_INPUT':
      const date = new Date(action.date);
      date.setDate(1);

      return {
        ...store,
        currentDate: date,
        events: {
          ...store.events,
          [formatKeyForDate(action.date)]: {
            title: action.title
          }
        }
      };
    default:
      return store;
  }
}
