// @flow
function isLeapYear(year: number): boolean {
  return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

const daysInMonthMap = {
  '0': 31,
  '1': year => (isLeapYear(year) ? 29 : 28),
  '2': 31,
  '3': 30,
  '4': 31,
  '5': 30,
  '6': 31,
  '7': 31,
  '8': 30,
  '9': 31,
  '10': 30,
  '11': 31,
};

function daysInMonth(year: number, month: number): number {
  return typeof daysInMonthMap[month] === 'function'
    ? daysInMonthMap[month](year)
    : daysInMonthMap[month];
}

export function generateCells(
  date: Date
): Array<{ year: number, month: number, day: number }> {
  let day = 1,
    month = date.getMonth(),
    year = date.getFullYear(),
    maxDays = daysInMonth(year, month);

  const weekDay = date.getDay();
  if (weekDay !== 1) {
    if (month === 0) {
      month = 11;
      year -= 1;
    } else {
      month -= 1;
    }
    maxDays = daysInMonth(year, month);
    day = weekDay === 0 ? maxDays - 5 : maxDays - weekDay + 2;
  }

  const cells = [];
  const maxRows = weekDay === 0 || weekDay === 6 ? 6 : 5;
  for (let rows = 0; rows < maxRows; rows++) {
    for (let cols = 0; cols < 7; cols++) {
      cells.push({
        year,
        month,
        day,
      });
      if (day === maxDays) {
        month = month === 11 ? 0 : month + 1;
        if (month === 0) {
          year += 1;
        }
        maxDays = daysInMonth(year, month);
        day = 1;
      } else {
        day += 1;
      }
    }
  }

  return cells;
}

const trim = v => v.trim();
const monthParts = [
  'янв',
  'фев',
  'мар',
  'апр',
  'мая',
  'июн',
  'июл',
  'авг',
  'сен',
  'окт',
  'ноя',
  'дек',
];

export function parseRawEventString(
  rawEvent: string
): { date: Date, title: string } | null {
  const parts = rawEvent.split(',').map(trim);
  if (parts.length < 2) {
    return null;
  }
  const date = new Date();
  const dateParts = parts[0].split(' ').map(trim);
  const day = parseInt(dateParts[0]);
  const month = dateParts[1];

  if (
    isNaN(day) ||
    !month ||
    !monthParts.includes(month.substr(0, 3)) ||
    !parts[1]
  ) {
    return null;
  }

  date.setDate(day);
  date.setMonth(monthParts.indexOf(month.substr(0, 3)));

  return {
    date,
    title: parts[1],
  };
}
