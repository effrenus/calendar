// @flow
export type Event = {
  title: string,
  participants: ?string[],
  description: ?string
};

export type Events = {
  [key: string]: Event
};

export type Store = {
  currentDate: Date,
  events: Events
};

export type Actions =
| {type: 'NEXT_MONTH'}
| {type: 'PREV_MONTH'}
| {type: 'ADD_OR_EDIT', index: number, date: Date }
| {type: 'SAVE', data: FormData, date: Date, cellIndex: number}
| {type: 'EDIT', data: FormData, date: Date, cellIndex: number}
| {type: 'DELETE', date: Date, cellIndex: number}
| {type: 'FAST_INPUT', date: Date, title: string}

export type ActionDispatcher = (Actions) => void;
